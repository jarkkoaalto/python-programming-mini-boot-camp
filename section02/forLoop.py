string_variable = "Hello world"
for char in string_variable:
    print(char)

list_var = ["Hello","world"]
print(list_var)

for item in list_var:
    print(item)

a = range(10)
print(a)

for num in a:
    print(num)